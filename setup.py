from setuptools import setup

setup(
    name='PIE665',
    version='',
    packages=['PI_E_665'],
    url='https://gitlab.com/dboonz/PIE665.CR',
    license='GNU GPLv2',
    author='Dirk Boonzajer',
    author_email='dboonz@gmail.com',
    description='Python interface for PI_E_665 positioner'
)
