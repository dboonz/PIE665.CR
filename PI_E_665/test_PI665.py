import unittest

from PI_E_665.PI665 import PI665
import logging

logging.basicConfig(level=logging.INFO)


class TestPI665(unittest.TestCase):
    def setUp(self) -> None:
        self.drive = PI665()

    def test_Position(self):
        print(self.drive.get_position())

    def test_idn(self):
        # for baudrate in [ 9600, 14400, 19200, 38400, 57600, 115200, 128000]:
        #     self.drive.close()
        #     self.drive = PI665(baudrate=baudrate)
        print(self.drive.IDN)

    def test_get_error(self):
        self.drive.get_error()

    def test_set_average_times(self):
        with self.assertRaises(AssertionError):
            self.drive.set_average_times(3)
        self.drive.set_average_times(4)

    def test_get_average_times(self):
        self.drive.get_average_times()


    def test_dco(self):
        with self.assertRaises(RuntimeError):
            self.drive.DCO = 1

    def test_position(self):
        pos = self.drive.position
        self.drive.SVO = 1
        if self.drive.OVF:
            print('OVERFLOW', self.drive.OVF)
        self.drive.position = 10 + pos

    def tearDown(self) -> None:
        self.drive.close()



