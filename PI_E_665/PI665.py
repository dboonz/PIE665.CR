import logging
import time

import serial

import atexit


error_dict = {0: 'No error',
              1: 'Parameter syntax error',
              2:'Unknown command',
              3:'Command lenth out of limits or buffer overrun',
              5:'Unallowable move attempted un unreferenced axis, or move attempted with servo off',
              10: 'Controller was stopped by command',
              15: 'Invalid axis identifier',
              17: 'Parameter out of range',
              20: 'Macro not found',
              54: 'Unknown parameter',
              56: 'Password invalid',
              60: 'Protected param: current command level too low',
              73: 'Motion commands are not allowed when wave generator is active'
              }


class SerialController(object):
    def __init__(self, port='/dev/ttyUSB0', baudrate=115200):
        self.logger = logging.getLogger(self.__name__)
        self.logger.setLevel(logging.INFO)
        # self.logger.debug('Logger started')
        self.serialport = serial.Serial(port=port, timeout=.1, baudrate=baudrate)
        atexit.register(self.serialport.close)
        atexit.register(self.close)
        self.logger.info(f'Connected to serial port {self.serialport.port}, baudrate {self.serialport.baudrate}')
        self.logger.info(f'Serial number: {self.identify()}')



    def ask(self, command):
        """ Send a command and return the response """
        if not command.endswith('\n'):
            command += '\n'
        self.send(command)
        return self.read()

    def ask_and_retry(self, command, numTries = 10):
        """ Send a command. If no response was given, try again for numTries times. """
        if numTries <= 0:
            raise ValueError(f'Could not get a response for {command}.')
        if numTries < 2:
            self.logger.warning(f'{numTries} left for command {command}')
        response = self.ask(command)
        if response == '':
            return self.ask_and_retry(command, numTries-1)
        else:
            return response

    def send(self, command:str):
        """ Send a command. """
        command = command.encode()
        self.logger.debug(f'Writing command {command} to serial.')
        self.serialport.write(command)

    def read(self, numLines=1):
        response = [self.serialport.readline().decode() for _ in range(numLines)]
        # self.logger.info(f'Response: {response}')
        response = '\n'.join(response)
        return response

    def close(self):
        self.serialport.close()

    def identify(self):
        raise NotImplementedError('Please implement identify in any subclasses.')





class PI665(SerialController):
    def __init__(self, *args, **kwargs):
        self.__name__ = 'PI 665.CR'
        super(PI665, self).__init__(*args, **kwargs)

        self.logger.setLevel(logging.INFO)
        # logging.basicConfig(level=logging.DEBUG)

        self.minpos = 1
        self.maxpos =  245

        # If this is turned on, setting the position results in waiting for it to converge
        self.wait_for_lock = True

    def identify(self):
        return self.IDN

    @property
    def IDN(self):
        return self.ask('*IDN?')

    def get_position(self):
        return float(self.ask_and_retry('POS? A\n'))

    def get_error(self):
        code = int(self.ask('ERR?'))
        if code != 0:
            try:
                err_msg = error_dict[code]
            except KeyError:
                err_msg = f'Error code {code} is not in list of known errors. (Look it up on page 41 of the manual)'
            self.logger.error(err_msg)
        return code

    def stop_all_axes(self):
        self.send('#24')
        self.get_error()

    def set_average_times(self, num_times=32):
        """
        Sets the number of samples to be used when calculating averages. Should be in
        [1,2,4,16,32,64].
        :param num_times:
        :return:
        """
        if num_times not in [1,2,4,8,16,32,64]:
            raise AssertionError(f' Num_times has to be within [1,2,4,8,16,32,64] or the drive may malfunction. Requested value: {num_times}.')
        self.send(f'AVG {num_times}')
        new_average = self.get_average_times()
        if new_average != num_times:
            raise RuntimeError(f'Something went wrong setting average time. Requested: {num_times}, but got {new_average} after setting.')

    def get_average_times(self, numTries=5):
        if numTries <= 0:
            raise RuntimeError('Could not get average times response')
        response = self.ask('AVG?')
        if response == '':
            return self.get_average_times(numTries=numTries-1)
        return int(response)

    @property
    def SVO(self):
        """ Set servo control"""
        response = self.ask_and_retry('SVO? A')
        return int(response)

    @SVO.setter
    def SVO(self,  new_value):
        if new_value not in [0,1]:
            raise AssertionError(f'SVO mode should be either 0 or 1, got {new_value}.')
        self.send(f'SVO A {new_value}')
        set_value = self.SVO
        if set_value != new_value:
            raise RuntimeError(f'Could not set SVO to {new_value}, got {set_value} after attempting to change.')
        return set_value

    @property
    def OVF(self):
        """ Get if overflow was detected. """
        return int(self.ask_and_retry('OVF? A'))


    @property
    def position(self):
        return self.get_position()

    @position.setter
    def position(self, new_value):
        # check that it's in computer controlled mode
        if new_value > self.maxpos or new_value < self.minpos:
            raise ValueError(f'Position has to be within {self.minpos} to {self.maxpos} micrometer. Got {new_value}.')
        # self.logger.info('Only use if board is in computer-controlled mode!')

        self._check_servo_controlled()
        self.ask(f'MOV A {new_value:.4f}')
        if self.wait_for_lock:
            while not self.on_target:
                time.sleep(0.05)


        return self.position

    @property
    def on_target(self):
        return int(self.ask_and_retry('ONT? A'))

    @property
    def DCO(self):
        """ Get Drift Compensation Operation mode mode"""
        return int(self.ask_and_retry('DCO? A'))

    @DCO.setter
    def DCO(self, new_value):
        raise RuntimeError('Somehow for this unit DCO cannot be set.')
        if new_value not in [0,1]:
            raise ValueError(f'DCO should be either 0 or 1. Got {new_value}.')
        self.send(f'DCO A {new_value}')
        returned_value = self.DCO
        if returned_value != new_value:
            raise ValueError(f'Could not set DCO. Requested {new_value} but after setting got {returned_value}')


    def _check_servo_controlled(self):
        if not self.SVO:
            raise RuntimeError('Device is not servo-controlled.')

