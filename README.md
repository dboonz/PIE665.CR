# python interface for PI 665.6

Python interface to the PI PIE 665.CR controller. No warranty whatsoever on any part, use at your own risk.

The tests are meant to visually inspect with the device running. However, the stage will be moved during the testing, so be careful when it's inside any actual setup. Again, no warranties given.
